<h1><?= getSubsection($content,"home::title") ?></h1>

<p>
    <?= anchor('/models'.($language=='en' ? '' : '-en').'/','switch ' . ($language=='en' ? 'naar Nederlands' : 'to English')) ?>
</p>


<p>
    <?= getSubsection($content,"home::introduction") ?>
</p>

<p>
    <?= getSubsection($content,"home::model_introduction") ?>
</p>

<?php if (! empty($models) && is_array($models)): ?>

    <?php foreach ($models as $models_item): ?>

        <div class="model-detail">

            <h2><?= getSubsection($model_content,"model::".$models_item['slug']."::title") ?></h2>

            <p><a href="/models/<?= esc($models_item['slug'], 'url') ?>"><?= getSubsection($content,"home::use_link") ?></a></p>

        </div>

    <?php endforeach ?>

<?php endif ?>

<p>
    <?= getSubsection($content,"home::closing_paragraph") ?>
</p>


