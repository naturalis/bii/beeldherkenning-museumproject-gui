<?= link_tag(base_url().'/css/upload.css'); ?>
<?= script_tag(base_url().'/js/upload.js'); ?>
<script type="text/javascript">

const allowedTypes = [ "image/png", "image/jpeg", "image/gif" ]

document.addEventListener("DOMContentLoaded", function()
{
    dropArea = document.getElementById('drop-area');
    fileListDisplay = document.getElementById('file-list');
    errorDisplay = document.getElementById('errors');
    resultsDisplay = document.querySelector("#results");
    imagePreview = document.getElementById("image_preview");
    uploadUrl = '/models/<?= $model['slug'] ?>/upload';
    submitOnUpload = true;

    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
    });

    ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, highlight, false)
    });

    ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false)
    });

    dropArea.addEventListener('drop', handleDrop, false)

    model_id = document.getElementById('model_id').value;
    upload_token = '<?= csrf_token() ?>';
    setUploadToken('<?= csrf_hash() ?>');
});

</script>
<h1>
    <?= getSubSubsection($content,"title") ?>
</h1>
<p class="model-id">
    Model ID: <?= $model["model_id"] ?>
</p>

<p>
    <?= anchor('/models/'.$model['slug'].'/'.($language=='en' ? 'nl' : 'en'),'switch ' . ($language=='en' ? 'naar Nederlands' : 'to English')) ?>
</p>

<?php if($model["settings"]["restricted"]!=1) { ?>

<p>
    <a href="/models/<?= esc($model['slug'], 'url') ?>/classes<?= ($language=='en' ? '-en' : '') ?>"><?= getSubSubsection($content,"classes_link") ?></a>
</p>
<?php } ?>

<p>
    <?= getSubSubsection($content,"body") ?>
</p>


<?php if($model["settings"]["restricted"]!=1) { ?>

<div class="wrapper" style="vertical-align: top;">
    <div  style="float:left;margin-right:25px;">
        <?= form_input('', esc($model['model_id']), ['id' => 'model_id'], 'hidden'); ?>
        <?= form_open('', ['class' => 'my-form']); ?>

            <p>
                <div id="drop-area">
                    <p><?= getSubsection($content,"upload::dropzone") ?></p>
                    <?= form_upload(['id'=>'fileElem','onchange' => 'handleFiles(this.files);']); ?>
                </div>
            </p>

            <?= form_label(getSubsection($content,"upload::button"), 'fileElem',['class'=>'button']); ?>

            <label><input type="checkbox" name="suppress_enrichments" id="suppress_enrichments">
                <?= getSubsection($content,"upload::suppress_enrichments") ?></label>

        <?= form_close(); ?>
    </div>

    <div id="preview" style="padding-top:15px">
        <img id="image_preview" src="">
    </div>

</div>

<div id="errors">

</div>

<br clear="all" />

<h3 class="results"><?= getSubSubsection($content,"results") ?></h3>
<div id="results" class="results">


</div>
<?php } else { ?>
    <?= getSubSubsection($content,"restricted") ?>
<?php } ?>