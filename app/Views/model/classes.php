<h1><?= getSubsection($content,"model::".$model['slug']."::title") ?></h1>

<p>
    <a href="/models/<?= esc($model['slug']) ?>">terug</a>
</p>

<?= getSubsection($content,"model::".$model['slug']."::classes_link") ?> (<?= count($classes); ?>):

<ul>
<?php foreach ($classes as $class): ?>
    <li><?= esc($class) ?></li>
<?php endforeach ?>
</ul>