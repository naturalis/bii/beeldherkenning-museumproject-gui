<h1>
    <?= getSubSubsection($content,"title") ?>
</h1>

<p>
    <?= anchor('/site/page/batch/'.($language=='en' ? 'nl' : 'en'),'switch ' . ($language=='en' ? 'naar Nederlands' : 'to English')) ?>
</p>

<p>
    <?= getSubSubsection($content,"body") ?>
</p>
