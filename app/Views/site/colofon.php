<h1><?= getSubsection($content,"colofon::title") ?></h1>

<p>
    <?= anchor('/site/page/colofon/'.($language=='en' ? 'nl' : 'en'),'switch ' . ($language=='en' ? 'naar Nederlands' : 'to English')) ?>
</p>

<p>
    <?= getSubsection($content,"colofon::body") ?>
</p>

