<style type="text/css">

table tbody tr td {
    width: 250px;
    height: 250px;
    vertical-align: top;
    text-align: left;
    padding-right: 10px;
}

img.thumb {
    max-width: 200px;
    max-height: 200px;
}

</style>
<h2>
    Afbeeldingen
</h2>
<p>

    <?= form_open_multipart('pages/upload') ?>
        <?= csrf_field() ?>
        <p>
            Afbeelding uploaden:
        </p>
        <p>
            <input type="file" name="userfile" size="20" /><br />
        </p>
        <p>
            <input type="submit" value="upload" />
        </p>
    </form>
</p>

<hr />

<p>
    Afbeelding gebruiken:<br />
    klik rechts op een foto, klik 'Copy image', ga naar de html-editor, klik rechts op de plaats in de content waar de afbeelding moet komen, en kies 'Paste'.
</p>

<hr />

<p>
<table>
    <tr>
<?php

    foreach ($images as $key => $value) {

        if (($key!=0) && ($key % 5)==0)
        {
            echo "</tr><tr>";
        }

        echo "<td>" . img(array('src'=>$value,'class'=>'thumb')) . "<br />" . basename($value) . "</td>";
    }
?>
    </tr>
</table>
</p>