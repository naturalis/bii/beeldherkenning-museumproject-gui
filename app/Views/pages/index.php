<p>
    Taal: <?= ($language=='en' ? 'Engels' : 'Nederlands') ?>
</p>
<p>
    <?= anchor('/pages'.($language=='en' ? '' : '-en').'/','switch naar ' . ($language=='en' ? 'Nederlands' : 'Engels')) ?>
</p>
<p>
    <?= anchor('/pages/images/','afbeeldingen','target="_images"') ?>
</p>
<p>
    secties:
<?php

    $array = null;
    $prev = null;

    foreach ($sections as $section)
    {
        $ied = explode("::", $section['section']);
        if ($ied[0]=="model") {
            $t = $ied[1];
        }
        else {
            $t = $ied[0];
        }

        if ($t!==$prev && $prev != null) {
            echo ul($array);
            $array = [];
        }

        $prev = $t;

        $c = array_filter($content,function($v) use($section) { return $v['section']==$section['section']; });
        if ($c) $c = " (" . substr(trim(strip_tags(array_pop($c)["content"])),0,25) . "&hellip;)"; else $c="";
        $array[] = anchor('/pages/edit/' . $section['id'] . '/' . $language,$section['section']) . $c;
    }

    echo ul($array);

?>
</p>
<p>

