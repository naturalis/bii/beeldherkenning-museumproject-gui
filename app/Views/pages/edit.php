<!-- script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script -->
<script src="/js/ckeditor/build/ckeditor.js"></script>

<style type="text/css">
.ck-editor__editable_inline {
    min-height: 350px;
}
</style>


<?= session()->getFlashdata('error') ?>
<?= service('validation')->listErrors() ?>
<?php

    $hidden = ['section' => $page['section'],'id' => $page['id'],'lang' => $language];
    echo form_open('/pages/save', ['name' => 'theForm'], $hidden);

?>
<?= csrf_field() ?>

<p>
    Taal: <?= ($language=='en' ? 'Engels' : 'Nederlands') ?>
</p>


<p>
    <?= anchor('/pages'.($language=='en' ? '-en' : '') .'/','index'); ?>
</p>
<p>
    Sectie: <?= $page['section'] ?>
</p>
<div style="width:1000px;">
    <textarea id="editor" name="content">
        <?= isset($content["content"]) ? $content["content"] : null ?>
    </textarea>
</div>
<script>

    // ClassicEditor
    //     .create( document.querySelector( '#editor' ), { licenseKey: '', } )
    //     .then( editor => { window.editor = editor; } )
    //     .catch( error => { console.error( error ); } );


    ClassicEditor
        .create( document.querySelector( '#editor' ), {
            licenseKey: '',
            link: {
                decorators: {
                    isExternal: {
                        mode: 'manual',
                        label: 'Open in a new tab',
                        defaultValue: true,
                        attributes: {
                            target: '_blank',
                            rel: 'noopener noreferrer'
                        }
                    }
                }
            }
        } )
        .then( editor => { window.editor = editor; } )
        .catch( error => { console.error( error ); } );


</script>
<p>
    <input type="submit" name="submit" value="opslaan" />
</p>
</form>