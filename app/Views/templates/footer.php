</div>
<div class="frame footer">
    <div style="margin-bottom: 10px;">
        <img class="logo naturalis" alt="Naturalis Biodiversity Center" src="/assets/images/logos/logo_Naturalis_rood_rgb_249-66-58.png">
        <img class="logo museon" alt="Museon-Omniversum" src="/assets/images/logos/Museon-Omniversum_Logo_colour_RGB.png">
        <img class="logo nmr" alt="Natuurhistorisch Museum Rotterdam" src="/assets/images/logos/logo_Het_Natuurhistorisch.jpeg">
        <img class="logo nmb" alt="Natuurmuseum Brabant" src="/assets/images/logos/logo_nmb.png">
        <img class="logo rijks" alt="Rijksmuseum" src="/assets/images/logos/rijksmuseum-logo.png">
    </div>
    <div>
        Mede mogelijk gemaakt door
    </div>
    <div>
        <img class="logo mondriaan" alt="Naturalis Biodiversity Center" src="/assets/images/logos/mondriaan.png">
        <img class="logo nlbif" alt="Museon-Omniversum" src="/assets/images/logos/nlbif.png">
        <img class="logo bernhard" alt="Natuurhistorisch Museum Rotterdam" src="/assets/images/logos/prins_bernhard.png">
    </div>
</div>
</body>
</html>






