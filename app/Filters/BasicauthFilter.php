<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\UserModel;


class BasicauthFilter implements FilterInterface
{

    private $users = null;

    public function __construct()
    {
        $this->users = model(UserModel::class);
    }

    public function before(RequestInterface $request, $arguments = null)
    {

        $username = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : "";
        $password = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : "";

        $user = $this->users->getUser($username,$password);

        if ((!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) || empty($user)) {
            header('WWW-Authenticate: Basic realm="Beeldherkenning"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Hit refresh';
            exit;
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}