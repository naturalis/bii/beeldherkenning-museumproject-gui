<?php

    function getSubsection($content,$subsection) {
        $p = @array_filter($content,function($v) use ($subsection) { return $v->section==$subsection;});
        if ($p) {
            return array_pop($p)->content;
        }
        else {
            return $subsection;
        }
    }

    function getSubSubsection($content,$subsubsection) {
        $p = @array_filter($content,function($v) use ($subsubsection) {
            $ied = explode('::', $v->section);
            return end($ied)==$subsubsection;
        });
        if ($p) {
            return array_pop($p)->content;
        }
        else {
            return $subsubsection;
        }
    }

    function foldMaskerClass($class)
    {
        return str_replace(['ë',' '], ['e',''],strtolower($class));
    }

?>