<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class Custom extends BaseConfig
{
    public $uploadAllowedMime  = 'image/jpeg,image/png,image/gif';
    public $uploadAllowedMimeBatch  = 'application/zip,text/tab-separated-values,text/csv,text/plain';
    public $uploadMaxSize  = '10000'; // kB
    public $uploadMaxDims  = '3840,2160';
    // public $uploadMaxSizeBatch  = '50000'; // kB
    public $apiBaseUrl =  'https://museum.identify.biodiversityanalysis.nl/api';
    public $apiIndexPath = '/models';
    public $deleteUploads = true;
    public $gbifSpeciesApiUrl = "https://api.gbif.org/v1/species/match?name=%CLASS%";
    public $gbifSpeciesUrl = "https://www.gbif.org/species/%USAGEKEY%";
}