<?php

namespace App\Controllers;

use App\Models\ModelsModel;
use App\Models\ContentModel;
use App\Models\MaskerModel;
use CodeIgniter\Files\File;

class Model extends BaseController
{

    private $model = null;
    private $curlOptions = [
        'timeout'  => 10,
        'allow_redirects' => true
    ];
    private $content = null;
    private $enrich_top = 3;

    public function __construct()
    {
        helper('html');
        helper('form');
        helper('toolbox');

        $this->content = model(ContentModel::class);
        $this->config = new \Config\Custom();
    }

    public function index($lang='nl')
    {
        $model = model(ModelsModel::class);

        $models = $model->getModel();

        $data = [
            'models'  => $models,
            'model_content'  => $this->content->getContentForPage($lang,"model"),
            'content' => $this->content->getContentForPage($lang,"home"),
            'language' => $lang
        ];

        echo view('templates/header', $data);
        echo view('templates/menu', $data);
        echo view('model/overview', $data);
        echo view('templates/footer', $data);
    }

    public function view($slug = null,$lang='nl')
    {
        $this->setModel($slug);

        $data['model'] = $this->model;
        $data['content'] = array_merge(
            $this->content->getContentForPage($lang,"model::" . $slug),
            $this->content->getContentForPage($lang,"upload::")
        );
        $data['language'] = $lang;

        echo view('templates/header', $data);
        echo view('templates/menu', $data);
        echo view('model/view', $data);
        echo view('templates/footer', $data);
    }

    public function classes($slug = null,$lang='nl')
    {
        $this->setModel($slug);

        $data['model'] = $this->model;
        $data['classes'] = $this->callApiClasses()->classes;
        $data['model']['slug'] = $slug;
        $data['content']  = $this->content->getContentForPage($lang,"model::" . $slug);

        echo view('templates/header', $data);
        echo view('templates/menu', $data);
        echo view('model/classes', $data);
        echo view('templates/footer', $data);
    }

    public function upload($slug = null)
    {
        // log_message('error',$this->request->getVar('model_id'));
        $suppress_enrichments = $this->request->getVar('suppress_enrichments')=='on';
        // log_message('error',$suppress_enrichments);

        $img = $this->request->getFile('userfile');

        if (is_null($slug))
        {
            $data = ['errors' => ['userfile' => 'No or empty slug.', 'file' => $img->getClientName() ] ];
            return view('model/upload_error', $data);
        }

        $this->setModel($slug);

        if ($this->model['model_id']!=$this->request->getVar('model_id'))
        {
            $data = ['errors' => [
                'userfile' => 'Model ID mismatch.',
                'file' => $img->getClientName(),
                'slug' => $slug,
                'model_id' => $this->request->getVar('model_id')
            ] ];
            return view('model/upload_error', $data);
        }

        $validationRule = [
            'userfile' => [
                'label' => 'Image File',
                'rules' => 'uploaded[userfile]'
                    . '|is_image[userfile]'
                    . '|mime_in[userfile,' . $this->config->uploadAllowedMime . ']'
                    . '|max_size[userfile,' . $this->config->uploadMaxSize . ']'
                    . '|max_dims[userfile,' . $this->config->uploadMaxDims . ']',
            ],
        ];

        if (!$this->validate($validationRule))
        {
            $data = ['errors' => [ 'error' => implode(":",$this->validator->getErrors())]];
            $data['errors']['file'] = $img->getClientName();

            return view('model/upload_error', $data);
        }

        if (!$img->hasMoved())
        {
            $filepath = WRITEPATH . 'uploads/' . $img->store();

            $f = new File($filepath);

            $response = $this->callApiIdentify($f->getRealPath());

            $this->resultData = [
                "file" => $f->getBasename(),
                "results" => $response,
                "enrichments" => [],
                "token" => csrf_hash()
            ];

            if (!$suppress_enrichments)
            {
                if ($this->model["slug"]=="maskers")
                {
                    $this->getEnrichmentsMaskers();
                }
                else
                {
                    $this->getEnrichments();
                }
            }
            if ($this->config->deleteUploads)
            {
                unlink($f->getRealPath());
            }

            return $this->response->setJSON($this->resultData);
        }
        else
        {
            $data = ['errors' => ['userfile' => 'The file has already been moved.', 'file' => $img->getClientName() ] ];
            return view('model/upload_error', $data);
        }
    }

    protected function setModel($slug)
    {
        $model = model(ModelsModel::class);
        $this->model = $model->getModel($slug);
        if (empty($this->model)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the model: ' . $slug);
        }
    }

    protected function callApiClasses()
    {
        $client = \Config\Services::curlrequest($this->curlOptions);

        $url = $this->config->apiBaseUrl . "/" . $this->model['api_selector'] . "/classes";

        $response = $client->request('GET', $url );

        if (strpos($response->getHeader('content-type'), 'application/json') !== false)
        {
            $body = json_decode($response->getBody());
        }
        else
        {
            $body = $response->getBody();
        }

        return $body;
    }

    protected function callApiIdentify($filepath)
    {
        $client = \Config\Services::curlrequest($this->curlOptions);

        $url = $this->config->apiBaseUrl . "/" . $this->model['api_selector'] . "/identify";

        $post_data = [
            'image' => new \CURLFile($filepath),
        ];

        $response = $client->request('POST', $url, [
            // 'auth' => ["wrn-01","L-z5GYEL5K"],
            'multipart' => $post_data
        ]);

        if (strpos($response->getHeader('content-type'), 'application/json') !== false)
        {
            $body = json_decode($response->getBody());
        }
        else
        {
            $body = $response->getBody();
        }

        return $body;
    }

    protected function callEnrichmentUrl($url)
    {
        $client = \Config\Services::curlrequest($this->curlOptions);

        $response = $client->request('GET',$url);

        if (strpos($response->getHeader('content-type'), 'application/json') !== false)
        {
            $body = json_decode($response->getBody());
        }
        else
        {
            $body = $response->getBody();
        }

        return $body;
    }

    protected function getEnrichmentsMaskers()
    {

        $masker = model(MaskerModel::class);

        foreach ($this->resultData["results"]->predictions as $key => $value)
        {
            if ($key >= $this->enrich_top)
            {
                // break;
            }

            $enrichment = [
                "body" => null,
                "link" => null,
                "gbif_link" => null,
                "worms_link" => null,
            ];

            $c = foldMaskerClass($value->class);
            $a = $masker->getRecordByFoldedClass($c);
            if (isset($a["url"]))
            {
                $enrichment["link"] = $a["url"];

                $this->resultData["enrichments"][] = [
                        "classname" => $value->class,
                        "enrichments" => $enrichment
                    ];
            }
        }
    }

    protected function getEnrichments()
    {
        foreach ($this->resultData["results"]->predictions as $key => $value)
        {
            if ($key >= $this->enrich_top)
            {
                break;
            }

            $enrichment = [
                "body" => null,
                "link" => null,
                "gbif_link" => null,
                "worms_link" => null,
            ];

            if (isset($this->model["settings"]["lng_embed"]))
            {
                $url = str_replace(
                    '%CLASS%',
                    rawurlencode($value->class),
                    $this->model["settings"]["lng_base"] . $this->model["settings"]["lng_embed"]);

                $t = $this->callEnrichmentUrl($url);
                if (isset($t->page) && isset($t->page->body)) {
                    $enrichment["body"] = $t->page->body;
                }
                // "title" => $enrichment->page->title,
            }

            if (isset($this->model["settings"]["lng_taxon_lookup"]))
            {
                $url = str_replace(
                    '%CLASS%',
                    rawurlencode($value->class),
                    $this->model["settings"]["lng_base"] . $this->model["settings"]["lng_taxon_lookup"]);

                $t = $this->callEnrichmentUrl($url);

                if (isset($this->model["settings"]["lng_link"]) && isset($t->taxon) && isset($t->taxon->id)) {
                    $enrichment["link"] = $this->model["settings"]["lng_base"] .
                        str_replace('%ID%', $t->taxon->id, $this->model["settings"]["lng_link"]);
                }
                elseif (isset($t->taxon) && isset($t->taxon->url)) {
                    $enrichment["link"] = $t->taxon->url;
                }
            }
            // else
            // if (isset($this->model["settings"]["other_embed"]))
            // {
            //     $url = str_replace('%CLASS%',rawurlencode($value->class),$this->model["settings"]["other_embed"]);
            //     // $enrichment = $this->callEnrichmentUrl($url);
            // }
            // else
            // if (isset($this->model["settings"]["other_link"]))
            // {
            //     $url = str_replace('%CLASS%',rawurlencode($value->class),$this->model["settings"]["other_link"]);
            //     // $enrichment = $this->callEnrichmentUrl($url);
            // }

            if (isset($this->model["settings"]["GBIF"]) && $this->model["settings"]["GBIF"]==1)
            {
                $url = str_replace('%CLASS%', rawurlencode($value->class), $this->config->gbifSpeciesApiUrl);
                $gbif = $this->callEnrichmentUrl($url);
                if ($gbif->usageKey)
                {
                    $enrichment["gbif_link"] = str_replace("%USAGEKEY%", $gbif->usageKey, $this->config->gbifSpeciesUrl);
                }
            }

            if (isset($this->model["settings"]["WORMS"]) && $this->model["settings"]["WORMS"]==1)
            {
                //  no WoRMS
            }

            $this->resultData["enrichments"][] = [
                "classname" => $value->class,
                "enrichments" => $enrichment
            ];
        }
    }

    protected function getApiModels()
    {
        $client = \Config\Services::curlrequest($this->curlOptions);

        $url = $this->config->apiBaseUrl . $this->config->apiIndexPath;

        $response = $client->request('GET', $url);

        if (strpos($response->getHeader('content-type'), 'application/json') !== false)
        {
            $body = json_decode($response->getBody(),true);
        }
        else
        {
            $body = $response->getBody();
        }

        return $body;
    }

}
