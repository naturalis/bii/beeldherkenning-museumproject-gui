<?php

namespace App\Controllers;

use App\Models\ContentModel;

class Site extends BaseController
{

    private $content = null;

    public function __construct()
    {
        helper('html');
        helper('form');
        helper('toolbox');

        $this->content = model(ContentModel::class);
        $this->config = new \Config\Custom();
    }

    public function page($page=false,$lang='nl')
    {
        if ($page==false)
        {
            return redirect('/');
        }

        $data['content'] = $this->content->getContentForPage($lang,$page);
        $data['language'] = $lang;

        echo view('templates/header');
        echo view('templates/menu');
        echo view('site/' . $page, $data);
        echo view('templates/footer');
    }

}
