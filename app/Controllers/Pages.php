<?php

namespace App\Controllers;

use App\Models\SectionsModel;
use App\Models\ContentModel;
use CodeIgniter\Files\File;

class Pages extends BaseController
{

    private $image_path = "assets/content-images/";

    public function __construct()
    {
        helper('html');
        helper('form');
        helper('filesystem');
        $this->config = new \Config\Custom();
    }

    public function index($lang='nl')
    {
        $sections = model(SectionsModel::class);
        $content = model(ContentModel::class);

        $data['sections']  = $sections->getSection();
        $data['default_page']  = 1;
        $data['content'] = $content->getContent($lang);
        $data['language'] = $lang;

        echo view('templates/header', $data);
        echo view('pages/index', $data);
        echo view('templates/footer', $data);
    }

    public function edit($section=1,$lang='nl')
    {
        $sections = model(SectionsModel::class);
        $content = model(ContentModel::class);

        $data['page'] = $sections->getSection($section);
        $data['content'] = $content->getContent($lang,$data['page']['section']);
        $data['language'] = $lang;

        echo view('templates/header', $data);
        echo view('pages/edit', $data);
        echo view('templates/footer', $data);
    }

    public function save()
    {
        $content = model(ContentModel::class);

        if ($this->request->getMethod() === 'post' && $this->validate([
            // 'title' => 'required|min_length[3]|max_length[255]',
            'section'  => 'required',
            'content'  => 'required',
            'lang'  => 'required',
        ])) {

            $content->where(['language' => $this->request->getPost('lang'),'section'=> $this->request->getPost('section')])->delete();

            $content->save([
                'language' => $this->request->getPost('lang'),
                'section' => $this->request->getPost('section'),
                'content'  => $this->request->getPost('content'),
            ]);
        }

        return redirect()->to('pages' . ($this->request->getPost('lang')=='en' ? '-en' : ''));
    }

    public function images()
    {
        $path = $this->image_path;

        $data["images"] =
            array_map(function($a) use ($path) { return $path . $a ;},
            array_filter(directory_map($this->image_path),function($a)
            {
                $path_info = pathinfo($a);
                return in_array($path_info['extension'], ["jpg","jpeg","png","gif"]);
            }));

        echo view('templates/header');
        echo view('pages/images',$data);
        echo view('templates/footer');
    }

    public function upload($slug = null)
    {
        $img = $this->request->getFile('userfile');

        $validationRule = [
            'userfile' => [
                'label' => 'Image File',
                'rules' => 'uploaded[userfile]'
                    . '|is_image[userfile]'
                    . '|mime_in[userfile,' . $this->config->uploadAllowedMime . ']'
                    . '|max_size[userfile,' . $this->config->uploadMaxSize . ']'
                    . '|max_dims[userfile,' . $this->config->uploadMaxDims . ']',
            ],
        ];

        if (!$this->validate($validationRule))
        {
            $data = ['errors' => $this->validator->getErrors()];
            $data['errors']['file'] = $img->getClientName();

            return view('pages/upload_error', $data);
        }

        if (!$img->hasMoved())
        {
            $img->move(WRITEPATH . 'uploads/content-images', $img->getClientName());
            $data = ['uploaded_flleinfo' => $img, 'filename' => $img->getClientName()];
            return redirect()->to('pages/images');
        }
        else
        {
            $data = ['errors' => ['userfile' => 'The file has already been moved.', 'file' => $img->getClientName() ] ];
            return view('pages/upload_error', $data);
        }
    }

}
