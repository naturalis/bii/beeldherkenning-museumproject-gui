<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class MaskerModel extends Model
{
    protected $table = 'masker_links';
    protected $allowedFields = [ 'class', 'class_lookup', 'link' ];

    public function getRecordByClass($class=false)
    {
        if ($class === false) {
            return;
        }

        return $this->where(['class' => $class])->first();
    }

    public function getRecordByFoldedClass($class=false)
    {
        if ($class === false) {
            return;
        }

        return $this->where(['class_lookup' => $class])->first();
    }
}
