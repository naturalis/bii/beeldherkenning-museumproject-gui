<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class UserModel extends Model
{
    protected $table = 'users';
    protected $allowedFields = [ 'name', 'email', 'password', 'created_at' ];

    public function getUser($name=false,$password=false)
    {
        if ($name === false || $password === false) {
            return;
        }

        return $this->where(['name' => $name,'password' => md5($password)])->first();
    }
}
