<?php

namespace App\Models;

use CodeIgniter\Model;

class EnrichmentsModel extends Model
{
    protected $table = 'enrichments';
    protected $allowedFields = ['type', 'title', 'content','classname','model_id'];

    public function getEnrichments($model_id = false,$classname = false)
    {
        if ($model_id === false) {
            throw new \CodeIgniter\Exceptions\DatabaseException('Need a model ID');
        }

        if ($classname === false) {
            throw new \CodeIgniter\Exceptions\DatabaseException('Need a classname');
        }

        // return $this->where(['slug' => $slug])->first();
        return $this->where(['model_id' => $model_id,'classname' => $classname])->findAll();
    }
}
