<?php

namespace App\Models;

use CodeIgniter\Model;

class ModelsModel extends Model
{
    protected $table = 'models';
    protected $allowedFields = ['title','slug','description','description_long','model_id','url'];

    public function getModel($slug = false)
    {
        if ($slug === false) {
            return $this->findAll();
        }

        $model =  $this->where(['slug' => $slug])->first();

        if ($model["settings"]) {
            $model["settings"] = json_decode($model["settings"],true);
        }

        return $model;
    }
}
