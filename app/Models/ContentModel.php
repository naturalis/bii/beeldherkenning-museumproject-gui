<?php

namespace App\Models;

use CodeIgniter\Model;

class ContentModel extends Model
{
    protected $table = 'content';
    protected $allowedFields = ['section','content','language'];


    protected function filterLanguage($d,$lang)
    {
        // sorry, it's getting late
        $a = [];

        foreach ($d as $val) {

            if ((is_object($val) ? $val->language : $val['language'])==$lang) {
                $a[] = $val;
            }

        }

        return $a;
    }

    public function getContent($lang,$section = false)
    {

        if ($section === false) {
            return $this->filterLanguage($this->findAll(),$lang);
        }

        return $this->where(['language' => $lang,'section' => $section])->first();
    }

    public function getContentForPage($lang,$page = false)
    {
        if ($page === false) {
            return $this->filterLanguage($this->findAll(),$lang);
        }

        $builder = $this->db->table($this->table);
        $builder->like('section', $page, 'after');
        $p = $builder->get()->getResult();

        $a = $this->filterLanguage($p,$lang);


        array_walk( $a,
            function(&$v,$k)
            {
                $v->content = preg_replace(['/^<p>/','/<\/p>$/'], '', $v->content);
                $v->content = preg_replace('/^<span style="color:#000000;">/', '<span>', $v->content);
            }
        );

        return $a;
    }
}
