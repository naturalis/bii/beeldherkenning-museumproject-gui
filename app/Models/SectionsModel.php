<?php

namespace App\Models;

use CodeIgniter\Model;

class SectionsModel extends Model
{
    protected $table = 'sections';
    protected $allowedFields = ['section','description'];

    public function getSection($id = false)
    {
        if ($id === false) {
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }
}
