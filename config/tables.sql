CREATE TABLE models (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    model_id VARCHAR(32) NOT NULL,
    title VARCHAR(128) NOT NULL,
    description VARCHAR(128) NOT NULL,
    description_long text,
    slug VARCHAR(128) NOT NULL,
    url_identify VARCHAR(255) NOT NULL,
    url_classes VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    KEY model_id (model_id),
    KEY slug (slug)
);

CREATE TABLE sections (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    section VARCHAR(128) NOT NULL,
    description VARCHAR(256),
    PRIMARY KEY (id),
    KEY section (section)
);

CREATE TABLE content (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    section VARCHAR(128) NOT NULL,
    content text,
    PRIMARY KEY (id),
    UNIQUE KEY section (section)
);

alter table content add column language varchar(2) not null default 'nl';
alter table content drop key section;
alter table content add unique key section_language (section,language);

CREATE TABLE users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(150),
    email VARCHAR(150),
    password VARCHAR(150),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE masker_links (
    id INT PRIMARY KEY AUTO_INCREMENT,
    class VARCHAR(256),
    class_lookup VARCHAR(256),
    url VARCHAR(1024),
    UNIQUE KEY class (class),
    UNIQUE KEY class_lookup (class_lookup)
);
