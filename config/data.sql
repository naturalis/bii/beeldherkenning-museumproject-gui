CREATE TABLE models (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    model_id VARCHAR(32) NOT NULL,
    name VARCHAR(128) NOT NULL,
    slug VARCHAR(128) NOT NULL,
    api_selector VARCHAR(64) NOT NULL,
    settings VARCHAR(2048),
    PRIMARY KEY (id),
    KEY model_id (model_id),
    KEY slug (slug)
);

-- 2022-05-11 15:00:06,184 - API - INFO - loaded 'Papilionidae' from /data/museum/naturalis/models/20220502-081233/; selector: papilionidae
-- 2022-05-11 15:00:08,005 - API - INFO - loaded 'Conidae' from /data/museum/conidae/models/20220426-141146/; selector: conidae
-- 2022-05-11 15:00:09,587 - API - INFO - loaded 'Maskers' from /data/museum/museon/maskers/models/20220510-141756/; selector: maskers
-- 2022-05-11 15:00:11,279 - API - INFO - loaded 'Maskers per continent' from /data/museum/museon/maskers/models/20220428-134153/; selector: maskers_continent
-- 2022-05-11 15:00:12,857 - API - INFO - loaded 'Vogeleieren' from /data/museum/bird_eggs/models/20220308-092837/; selector: vogeleieren
-- 2022-05-11 15:00:14,553 - API - INFO - loaded 'Eierkapsels haaien & roggen' from /data/museum/shark_eggs/models/20220506-095416/; selector: haaien_roggen_eieren

insert into models (model_id,name,slug,api_selector,settings) values
    ("20220502-081233",
        "Papilionidae","papilionidae","papilionidae",
        '{  "lng_base" : "https://bseai.linnaeus.naturalis.nl/linnaeus_ng/app/views",
            "lng_embed" : "/webservices/taxon_page.php?pid=1&cat=1&taxon=%CLASS%",
            "lng_taxon_lookup" : "/webservices/taxon.php?pid=1&taxon=%CLASS%",
            "lng_link" : "/species/nsr_taxon.php?id=%ID%&cat=TAB_DESCRIPTION&epi=1",
            "GBIF": 1, "WORMS": 0, "restricted" : 0}'),

    ("20220502-081233",
        "Papilionidae","papilionidae_subfamilies","papilionidae_subfamilies",
        '{  "GBIF": 1, "WORMS": 0, "restricted" : 0}'),

    ("20220502-081233",
        "Papilionidae","papilionidae_genera","papilionidae_genera",
        '{  "GBIF": 1, "WORMS": 0, "restricted" : 0}'),

    ("20220426-141146",
        "Conidae","conidae","conidae",
        '{"GBIF": 1, "WORMS": 1, "restricted" : 0}'),

    ("20220510-141756",
        "Maskers","maskers","maskers",
        '{ "GBIF": 0, "WORMS": 0, "restricted" : 0 }'),

    ("20220510-141756",
        "Maskers per continent","maskers_continent","maskers_continent",
        '{ "GBIF": 0, "WORMS": 0, "restricted" : 0 }'),
    ("20220308-092837",
        "Vogeleieren","vogeleieren","vogeleieren",
        '{  "lng_base" : "https://www.nederlandsesoorten.nl/linnaeus_ng/app/views",
            "lng_embed" : "/webservices/taxon_page.php?pid=1&taxon=%CLASS%&cat=163",
            "lng_taxon_lookup" : "/webservices/taxon.php?pid=1&taxon=%CLASS%",
            "lng_link" : "/species/nsr_taxon.php?id=%ID%&cat=TAB_DESCRIPTION&epi=1",
            "GBIF": 1, "WORMS": 0, "restricted" : 1 }'),
    ("20220506-095416",
        "Eierkapsels haaien & roggen","haaien_roggen_eieren","haaien_roggen_eieren",
        '{  "lng_base" : "https://www.nederlandsesoorten.nl/linnaeus_ng/app/views",
            "lng_embed" : "/webservices/taxon_page.php?pid=1&taxon=%CLASS%&cat=156",
            "lng_taxon_lookup" : "/webservices/taxon.php?pid=1&taxon=%CLASS%",
            "lng_link" : "/species/nsr_taxon.php?id=%ID%&cat=TAB_DESCRIPTION&epi=1",
            "GBIF": 1, "WORMS": 1, "restricted" : 0 }');


insert into sections (section,description) values
    ("home::title","homepage"),
    ("home::introduction","homepage"),
    ("home::model_introduction","homepage"),
    ("home::model_title::papilionoidea","homepage"),
    ("home::model_title::conidae","homepage"),
    ("home::model_title::ray_and_shark_eggs","homepage"),
    ("home::model_title::bird_eggs","homepage"),
    ("home::model_title::masks","homepage"),
    ("home::use_link","homepage"),
    ("home::closing_paragraph","homepage"),
    ("model::papilionidae::title","papilionidae model"),
    ("model::papilionidae::classes_link","papilionidae model"),
    ("model::papilionidae::body","papilionidae model"),
    ("model::papilionidae_subfamilies::title","papilionidae_subfamilies model"),
    ("model::papilionidae_subfamilies::classes_link","papilionidae_subfamilies model"),
    ("model::papilionidae_subfamilies::body","papilionidae_subfamilies model"),
    ("model::papilionidae_genera::title","papilionidae_genera model"),
    ("model::papilionidae_genera::classes_link","papilionidae_genera model"),
    ("model::papilionidae_genera::body","papilionidae_genera model"),
    ("model::conidae::title","conidae model"),
    ("model::conidae::classes_link","conidae model"),
    ("model::conidae::body","conidae model"),
    ("model::haaien_roggen_eieren::title","haaien_roggen_eieren model"),
    ("model::haaien_roggen_eieren::classes_link","haaien_roggen_eieren model"),
    ("model::haaien_roggen_eieren::body","haaien_roggen_eieren model"),
    ("model::vogeleieren::title","vogeleieren model"),
    ("model::vogeleieren::classes_link","vogeleieren model"),
    ("model::vogeleieren::body","vogeleieren model"),
    ("model::vogeleieren::restricted","vogeleieren model"),
    ("model::maskers::title","maskers model"),
    ("model::maskers::classes_link","maskers model"),
    ("model::maskers::body","maskers model"),
    ("model::maskers_continent::title","maskers_continent model"),
    ("model::maskers_continent::classes_link","maskers_continent model"),
    ("model::maskers_continent::body","maskers_continent model"),
    ("batch::title","homepage"),
    ("batch::body","homepage"),
    ("api::title","homepage"),
    ("api::body","homepage"),
    ("colofon::title","colofon"),
    ("colofon::body","colofon"),
    ("upload::dropzone","upload"),
    ("upload::button","upload"),
    ("upload::suppress_enrichments","upload"),
    ("upload::results","upload")
;

update content set section = 'model::vogeleieren::title' where section = 'home::model_title::bird_eggs';
update content set section = 'model::conidae::title' where section = 'home::model_title::conidae';
update content set section = 'model::maskers::title' where section = 'home::model_title::masks';
update content set section = 'model::papilionidae::title' where section = 'home::model_title::papilionoidea';
update content set section = 'model::haaien_roggen_eieren::title' where section = 'home::model_title::ray_and_shark_egg';

