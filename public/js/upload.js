// https://www.smashingmagazine.com/2018/01/drag-drop-file-uploader-vanilla-js/
var dropArea = null
var fileListDisplay = null
var errorDisplay = null
var resultsDisplay = null
var imagePreview  = null
var submitOnUpload = false
var uploadUrl = null
var upload_file = null
var errors = []
var model_id = null
var upload_token = null
var upload_hash = null
var resultData = {}
var formData = null
var maxResults = 3

function preventDefaults(e)
{
    e.preventDefault()
    e.stopPropagation()
}

function validateFile(file)
{
    if(file.length == 0)
    {
        errors.push("No file")
        return false
    }

    if(allowedTypes.indexOf(file.type) == -1)
    {
        errors.push({file:file.name,error:`file type '${file.type}' not allowed`})
        return false
    }

    return true
}

function handleDrop(e)
{
    let dt = e.dataTransfer
    handleFiles(dt.files)
}

function printSelectedFile()
{
    // document.getElementById('selected-file').innerHTML = upload_file ? upload_file.name : "";
}

function handleFiles(files)
{
    errors = []
    resultData = {}
    upload_file = null

    // printSelectedFile()
    showImagePreview()

    for(let file of files)
    {
        if (validateFile(file))
        {
            upload_file = file
            // printSelectedFile()
            uploadFile()
            showImagePreview()
            break
        }
    }

    printErrors()
}

function uploadFile()
{
    formData = new FormData()

    formData.append(upload_token, upload_hash)
    formData.append('userfile', upload_file)
    formData.append('model_id', model_id)
    const cb = document.querySelector('#suppress_enrichments');
    formData.append('suppress_enrichments', cb.checked ? 'on' : 'off');

    // for (var pair of formData.entries()) { console.log(pair[0]+ ', ' + pair[1]); }

    if (submitOnUpload) { fetchUrl() }
}

function setUploadToken(value)
{
    upload_hash = value;
    // console.log(upload_hash);
}

function fetchUrl()
{
    fetch(uploadUrl, {
        method: 'POST',
        mode: 'no-cors',
        cache: 'no-cache',
        body: formData,
        redirect: 'follow'
    })
    .then(response => response.text())
    .then(data => {

        try {
            // console.log(data)
            resultData = JSON.parse(data)
            // console.log(resultData)

            if (resultData.error) { throw resultData.error; }
            if (resultData.results.error) { throw resultData.results.error; }

            printResults()
            showResults()
            setUploadToken(resultData.token)

        } catch (error) {

            // console.log(data)
            console.log(error)

            if (data['error']) {
                err = data.error
            }
            else
            if (data['userfile']) {
                err = data.userfile
            }
            else {
                err = error
            }

            errors.push({file:formData.get('userfile').name,error: err})
            printErrors()
        }

    })
    .catch(error => {
        console.error(error);
    });
}

function showImagePreview()
{
    if (!upload_file) return;

    if([ "image/png", "image/jpeg", "image/gif" ].indexOf(upload_file.type) == -1) return;

    const reader = new FileReader();

    reader.onload = function(e)
    {
        const contents = e.target.result;
        imagePreview.src = contents;
    };

    reader.readAsDataURL(upload_file);
}


function highlight(e)
{
    dropArea.classList.add('highlight')
}

function unhighlight(e)
{
    dropArea.classList.remove('highlight')
}

function printResults()
{
    let tpl1 = '<ol>%BODY%</ol>';
    let tpl2 = '<li> \
            <div class="main-info"> \
                <div class="class">%CLASS%%INFOLINK%</div> \
                <div class="prediction %STATUS%">%PREDICTION%</div> \
                <div class="links">%LINKS%</div>\
            </div> \
            <div class="body" id="%INFOID%">%BODY%</div> \
        </li> \
        <br clear="all" />';
    let infotpl = '<span class="info-link" onclick="toggle(document.getElementById(\'%INFOID%\'))">&#9432;</span>';
    let linktpl = '<a href="%HREF%" target="_blank">%LINK%</a>';
    let buffer=[];
    let status = "high";

    for (var i=0; i < resultData.results.predictions.length; i++)
    {
        if (i>=maxResults)
        {
            break;
        }

        if (resultData.results.predictions[i].prediction>=0.95)
        {
            status = "high";
        }
        else
        if (resultData.results.predictions[i].prediction>=0.85)
        {
            status = "middle";
        }
        else
        {
            status = "low";
        }

        let links = [];
        let body = "";
        let infolink= "";
        let infoid = 'info-' + i;
        let e = resultData.enrichments.filter(enrichment => enrichment.classname==resultData.results.predictions[i].class);

        if (e.length>0)
        {
            e = e.pop();
            if (e.enrichments.body) body = e.enrichments.body;
            if (e.enrichments.link) links.push(linktpl.replace("%HREF%",e.enrichments.link).replace("%LINK%","meer informatie"));
            if (e.enrichments.gbif_link) links.push(linktpl.replace("%HREF%",e.enrichments.gbif_link).replace("%LINK%","GBIF"));
            if (e.enrichments.worms_link) links.push(linktpl.replace("%HREF%",e.enrichments.worms_link).replace("%LINK%","WoRMS"));
        }

        if (body.length>0)
        {
            infolink = infotpl.replace('%INFOID%',infoid);
        }

        buffer.push(
            tpl2
                .replace('%CLASS%',resultData.results.predictions[i].class)
                .replace('%INFOLINK%',infolink)
                .replace('%STATUS%',status)
                .replace('%PREDICTION%',resultData.results.predictions[i].prediction)
                .replace('%BODY%',body)
                .replace('%INFOID%',infoid)
                .replace('%LINKS%',links.join("\n"))
        );

    }

    resultsDisplay.innerHTML =  tpl1.replace('%BODY%',buffer.join("\n"));

}

function printErrors()
{
    let t_err = errors.map(function(val) { return val.error ? `"${val.file}": ${val.error}` : val; })
    errorDisplay.innerHTML = (t_err.join("<br />"));
}

function showResults()
{
    for(let item of document.getElementsByClassName('results'))
    {
        item.style.display ='block';
    }
}


function validateEmail(str)
{
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(str).toLowerCase());
}

function validateInput(ele)
{
    if (ele.name=='email')
    {
        var valid = validateEmail(ele.value);
    }

    for(let item of document.getElementsByClassName('validation ' + ele.name + ' valid'))
    {
        item.style.display = valid ? 'inline' : 'none';
    }

    for(let item of document.getElementsByClassName('validation ' + ele.name + ' invalid'))
    {
        item.style.display = valid ? 'none' : 'inline';
    }
}

function submitForm()
{
    validateEmail(document.getElementById('email').value) || errors.push("Invalid email address");

    if (!upload_file)
    {
        errors.push("No file");
    }

    if (errors.length>0)
    {
        let t_err = errors.map(function(val) { return val.error ? val.error : val; })
        printErrors()
        return false;
    }

    formData.append('email', document.getElementById('email').value)
    fetchUrl()
}

var show = function (elem) {
    elem.style.display = 'block';
};

// Hide an element
var hide = function (elem) {
    elem.style.display = 'none';
};

// Toggle element visibility
var toggle = function (elem) {

    // If the element is visible, hide it
    if (window.getComputedStyle(elem).display === 'block') {
        hide(elem);
        return;
    }

    // Otherwise, show it
    show(elem);
};
